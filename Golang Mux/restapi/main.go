package main

import (
"encoding/json"
"log"
"math/rand"
"net/http"
"strconv"
"github.com/gorilla/mux"
)

// Book struct(model)
type Book struct{
	ID string `json:"id"`
	Isbn string `json:"isbn"`
	Title string `json:"title"`
	Author *Author `json:"author"`
}

// Author Struct
type Author struct{
	Firstname string `json:"firstname"`
	Lastname string `json.:"lastname"`
}

// Init Books var a slice Book struct
var books []Book

//Get All books
func getBooks(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type","application/json")
	json.NewEncoder(w).Encode(books)
}

//Get single book
func getBook(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type","application/json")
	params := mux.Vars(r)
	//loop through book and file with id
	for _, item:= range books{
		if item.ID == params["id"]{
			json.NewEncoder(w).Encode(item)
		}
	}
	json.NewEncoder(w).Encode(&Book{})
}

//Create new book
func createBook(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type","application/json")
	var book Book
	_= json.NewDecoder(r.Body).Decode(&book)
	book.ID = strconv.Itoa(rand.Intn(10000000)) //Mock not-safe
	books = append(books, book)
	json.NewEncoder(w).Encode(book)
}

//Update
func updateBook(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-type", "application/json")
	params := mux.Vars(r)
	for index, item := range books {
		if item.ID == params["id"] {
			books = append(books[:index], books[index+1:]...)
			w.Header().Set("Content-type", "application/json")
			var book Book
			_ = json.NewDecoder(r.Body).Decode(&book)
			book.ID = params["id"] //generate new id
			books = append(books, book)
			return
		}
	}
	json.NewEncoder(w).Encode(books)
}

// Delete book
func deleteBook(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-type", "application/json")
	params := mux.Vars(r)
	for index, item := range books {
		if item.ID == params["id"] {
			books = append(books[:index], books[index+1:]...)
			break
		}
	}
	json.NewEncoder(w).Encode(books)
}

func main() {
	//Init Router
	r := mux.NewRouter()

	// Mock Data - implementasi DB
	books = append(books, Book{
		ID:"1",
		Isbn:"12123312",
		Title: "Bisa jadi",
		Author: &Author{
			Firstname:"Ludfi", Lastname:"Ni'am",
			},
			})

	books = append(books, Book{
		ID:"2",
		Isbn:"12121212",
		Title: "Masak Bisa",
		Author: &Author{
			Firstname:"Ahmad", Lastname:"Sujiwo",
			},
			})

	// Route handlers Endpoin
	r.HandleFunc("/api/books", getBooks).Methods("GET")
	r.HandleFunc("/api/books/{id}", getBooks).Methods("GET")
	r.HandleFunc("/api/books", createBook).Methods("POST")
	r.HandleFunc("/api/books/{id}", updateBook).Methods("PUT")
	r.HandleFunc("/api/books/{id}", deleteBook).Methods("DELETE")

	log.Fatal(http.ListenAndServe(":8000",r))

}