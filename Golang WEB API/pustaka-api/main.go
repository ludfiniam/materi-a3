package main

import(
"github.com/gin-gonic/gin"
"pustaka-api/hendler"
"gorm.io/driver/mysql"
"gorm.io/gorm"
"log"
"pustaka-api/book"
// "fmt"
)

func main() {
	dsn := "root:@tcp(127.0.0.1)/pustaka-api?charset=utf8&parseTime=True&loc=Local"
	db, err := gorm.Open(mysql.Open(dsn), &gorm.Config{})
	if err != nil{
		log.Fatal("Db connection error")
	}

	db.AutoMigrate(book.Book{})
	bookRepository := book.NewRepository(db)
	
	// bookFileRepository:= book.NewFileRepository()
	bookService := book.NewService(bookRepository)
	bookHendler := handler.NewBookHandler(bookService)


	// ============================================

	//=> Get All data
	// books, err := bookRepository.FindAll()
	// for _, book := range books{
	// 	fmt.Println("Title :", book.Title)
	// }

	//=> Get data Find by ID
	// books, err := bookRepository.FindByID(1)
	// fmt.Println("Title :", books.Title)

	//=> Create
	// BookInput := book.Book{
	// 	Title: "Coba lagi kawan",
	// 	Price:  14000,
	// }
	// bookService.Create(BookInput)

	//==============================================

	//=> Create
	// book := book.Book{
	// 	Title: "Au ah gelap",
	// 	Description: "Good book kayaknya",
	// 	Price: 56000,
	// 	Rating: 4,
	// 	Discount:7,
	// }

	// bookRepository.Create(book)
	// fmt.Println("Title :", book.Title)

	//==========CRUD===========
	//=> Create
	// book := book.Book{}
	// book.Title= "Tuhan maha Asyik"
	// book.Price= 192000
	// book.Discount = 40
	// book.Rating = 5
	// book.Description = "yang Asyik emang enak"

	// err = db.Create(&book).Error
	// if err != nil {
	// 	fmt.Println("==============")
	// 	fmt.Println("Error creating")
	// 	fmt.Println("==============")
	// }

	//==========================
	//=> Read imit 1 data and order by
	
	// var book book.Book
	// err = db.Debug().First(&book, 3).Error
	// if err != nil{
	// fmt.Println("==================")
	// fmt.Println("Error finding book")
	// fmt.Println("==================")
	// }

	// 	fmt.Println("Title :", book.Title)
	// 	fmt.Printf("book object %v\n", book.Title)

	//===========================
	//=> Read  select all

	// var books []book.Book
	// err = db.Debug().Find(&books).Error

	// if err != nil{
	// fmt.Println("==================")
	// fmt.Println("Error finding book")
	// fmt.Println("==================")
	// }

	// for _, b := range books{
	// 	fmt.Println("Title :", b.Title)
	// 	fmt.Printf("book object %v\n", b)
	// }

	//============================
	//=> Read where title= all

	// var books []book.Book
	// err = db.Debug().Where("title = ?", "Man Tiger").Find(&books).Error
	// if err != nil{
	// fmt.Println("==================")
	// fmt.Println("Error finding book")
	// fmt.Println("==================")
	// }

	// for _, b := range books{
	// 	fmt.Println("Title :", b.Title)
	// 	fmt.Printf("book object %v\n", b)
	// }

	//============================
	//=> Update

	// var book book.Book
	// err = db.Debug().Where("id = ?", 1).First(&book).Error
	// if err != nil{
	// 	fmt.Println("==================")
	// 	fmt.Println("Error finding book")
	// 	fmt.Println("==================")
	// }

	// book.Title = "Man Tiger(Vol:2)"
	// err = db.Save(&book).Error
	// if err != nil{
	// 	fmt.Println("==============")
	// 	fmt.Println("Error Updating")
	// 	fmt.Println("==============")
	// }

	//=============================
	//=> Delete data

	// var book book.Book
	// err = db.Debug().Where("id = ?", 4).First(&book).Error
	// if err != nil{
	// 	fmt.Println("==================")
	// 	fmt.Println("Error finding book")
	// 	fmt.Println("==================")
	// }

	// err = db.Delete(&book).Error
	// if err != nil{
	// 	fmt.Println("==============")
	// 	fmt.Println("Error Delete")
	// 	fmt.Println("==============")
	// }


	//================================



	router := gin.Default()

	v1 := router.Group("/v1")

	//Http->GET
	// router.GET("/", bookHendler.RootHendler)

	// v1.GET("/hello", bookHendler.HelloHanlerer)

	// v1.GET("/books/:id/:title",bookHendler.BooksHendlerer)

	// v1.GET("/query", bookHendler.QueryHendlerer)

	//Http->POST

	v1.POST("/books", bookHendler.CreateBook)
	v1.GET("/books", bookHendler.GetBooks)
	v1.GET("/books/:id", bookHendler.GetBook)
	v1.PUT("/books/:id", bookHendler.UpdateBook)
	v1.DELETE("/books/:id", bookHendler.DeleteBook)


	router.Run()
	
}



