package book

import("encoding/json")

type BookRequest struct{
	Title string `json:"title" binding:"required"`
	//identifikasi number input string jadi internal server error
	Price json.Number `json:"price" binding:"required,numeric"`	
	Description string `json:"description" binding:"required"`
	Rating json.Number `json:"rating" binding:"required,numeric"`
	Discount json.Number `json:"discount" binding:"required,numeric"`

}
