package handler

import(
"net/http"
"github.com/gin-gonic/gin"
"pustaka-api/book"
"github.com/go-playground/validator/v10"
"fmt"
"strconv"
)

type bookHendler struct{
	bookService book.Service
}

func NewBookHandler(bookService book.Service) *bookHendler{
	return &bookHendler{bookService}
}


// func (h *bookHendler) RootHendler(c *gin.Context) {
// 	c.JSON(http.StatusOK, gin.H{
// 		"name": "Ahmad Ludfi Niam",
// 		"bio": "A Quality Assurance Junior",
// 		})	
// }

// func (h *bookHendler) HelloHanlerer(c *gin.Context) {
// 	c.JSON(http.StatusOK, gin.H{
// 		"title": "Hello world",
// 		"Subtitle": "Belajar golang bareng Agung Setiawan",
// 		})	
// }

// func (h *bookHendler) BooksHendlerer(c *gin.Context) {
// 	id := c.Param("id")
// 	title := c.Param("title")
// 	c.JSON(http.StatusOK, gin.H{
// 		"id": id,
// 		"title": title,
// 		})
// }

// func (h *bookHendler) QueryHendlerer(c *gin.Context) {
// 	title := c.Query("title")
// 	price := c.Query("price")
// 	c.JSON(http.StatusOK, gin.H{
// 		"title": title,
// 		"price": price,
// 		})
// }

// All books
func (h *bookHendler) GetBooks(c *gin.Context){
	books, err := h.bookService.FindAll()
	if err != nil{
		c.JSON(http.StatusBadRequest, gin.H{
			"error": err,
			})
		return
	}

	var booksResponse []book.BookResponse

	for _, b := range books{
		bookResponse := converToResponse(b)
		booksResponse = append(booksResponse, bookResponse)
	}

	c.JSON(http.StatusOK, gin.H{
		"data": booksResponse,
		})
}

// Select book 1
func (h *bookHendler) GetBook(c *gin.Context){
	idString := c.Param("id")
	id, _:=strconv.Atoi(idString)
	b, err:= h.bookService.FindByID(int(id))

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": err,
			})
		return
	}

	bookResponse := converToResponse(b)

	c.JSON(http.StatusOK, gin.H{
		"data": bookResponse,
		})

}

func (h *bookHendler) CreateBook(c *gin.Context) {
	//data yang diterima title dan price
	var bookRequest book.BookRequest

	err := c.ShouldBindJSON(&bookRequest)
	if err != nil {
		errorMessages := []string{}
		for _, e := range err.(validator.ValidationErrors){
			errorMessage := fmt.Sprintf("Erro on field: %s, conditional: %s", e.Field(), e.ActualTag())
			errorMessages = append(errorMessages, errorMessage)
		}
		c.JSON(http.StatusBadRequest, gin.H{
			"error": errorMessages,
			})
		return
	}

	book, err := h.bookService.Create(bookRequest)

	if err != nil{
		c.JSON(http.StatusBadRequest, gin.H{
			"error": err,
			})
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"data": book,
		})
}


func (h *bookHendler) UpdateBook(c *gin.Context) {
	// title, price
	var bookRequest book.BookRequest

	err := c.ShouldBindJSON(&bookRequest)
	if err != nil {

		errorMessages := []string{}
		for _, e := range err.(validator.ValidationErrors) {
			errorMessage := fmt.Sprintf("Error on field %s, condition %s", e.Field(), e.ActualTag())
			errorMessages = append(errorMessages, errorMessage)

		}

		c.JSON(http.StatusBadRequest, gin.H{
			"errors": errorMessages,
			})
		return

	}

	idString := c.Param("id")
	id, _ := strconv.Atoi(idString)

	book, err := h.bookService.Update(id, bookRequest)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"errors": err,
			})
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"data": converToResponse(book),
		})
}

func (h *bookHendler) DeleteBook(c *gin.Context){
	idString := c.Param("id")
	id, _:=strconv.Atoi(idString)
	b, err:= h.bookService.Delete(int(id))

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": err,
			})
		return
	}

	bookResponse := converToResponse(b)

	c.JSON(http.StatusOK, gin.H{
		"data": bookResponse,
		})

}

func converToResponse(b book.Book) book.BookResponse{
	return book.BookResponse{
		ID: b.ID,
		Title: b.Title,
		Price: b.Price,
		Description: b.Description,
		Rating: b.Rating,
		Discount: b.Discount,
	}
}