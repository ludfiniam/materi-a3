package main 

import(
"github.com/julienschmidt/httprouter"
"gopkg.in/mgo.v2"
"net/http"
"github.com/akhil/mongo-golang/controllers"
)

func main() {
	r := httprouter.New()
	uc := controllers.NewUserController(getSession())

	r.GET("/users/:id", uc.GetUser)
	r.POST("/users", uc.CreateUser)
	r.DELETE("/users/:id", uc.DeleteUser)

	http.ListenAndServe(":9000", r)
}

func getSession() *mgo.Session {
	s, err := mgo.Dial("mongodb://HOST:PORT")
	if err != nil {
		panic(err)
	}
	return s
}